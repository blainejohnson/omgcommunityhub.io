{
  "attributes": [
    {
      "trait_type": "level",
      "value": 1
    },
    {
      "max_value": 100,
      "trait_type": "stamina",
      "value": 10
    },
    {
      "trait_type": "personality",
      "value": "brilliant"
    },
    {
      "display_type": "boost_number",
      "trait_type": "puff_power",
      "value": 10
    },
    {
      "display_type": "boost_percentage",
      "trait_type": "stamina_increase",
      "value": 5
    },
    {
      "display_type": "number",
      "trait_type": "generation",
      "value": 1
    }
  ],
  "description": "Unbank the banked",
  "externalUrl": "https://go-community.github.io/0x5.txt",
  "imageUrl": "https://pbs.twimg.com/media/DIC1NjkU0AAdILL.jpg",
  "name": "I Am Unbanked"
}
